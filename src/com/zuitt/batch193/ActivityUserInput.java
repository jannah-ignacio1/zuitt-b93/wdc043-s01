package com.zuitt.batch193;

import java.util.Scanner;

public class ActivityUserInput {

    public static void main(String[] args) {

        String firstName;
        String lastName;
        double firstSubjectGrade;
        double secondSubjectGrade;
        double thirdSubjectGrade;

        Scanner scan = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = scan.nextLine();

        System.out.println("Last Name:");
        lastName = scan.nextLine();

        System.out.println("First Subject Grade:");
        firstSubjectGrade = scan.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubjectGrade = scan.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubjectGrade = scan.nextDouble();

        //int ave = ((int)(firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade) / 3);

        double ave = ((firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade) / 3);

        System.out.println("Good day, " + lastName + " " + firstName + ".");
        System.out.println("Your grade average is: " + ave);
    }
}
